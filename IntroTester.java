package projects.psgraphintro;

import generating_input.PrologReader;

import java.io.File;

import making_concepts.BinaryProductionRule;
import modules.production_rules.logical.Conjunction;
import modules.production_rules.logical.Disjunction;
import modules.production_rules.logical.Instantiation;
import modules.production_rules.logical.Negation1;
import modules.production_rules.logical.Negation2;
import modules.production_rules.numerical.Count;
import theory.Theory;
import viewing_theories.ConceptViewer;
import viewing_theories.TheoryViewer;

public class IntroTester {

	public static void main(String[] args) {

		long ms1 = System.currentTimeMillis();
		Theory theory = new Theory();
		String intro_file_path = System.getProperty("user.dir") + File.separator
				+ "projects" + File.separator + "psgraphintro" + File.separator
				+ "intronew.pl";
		PrologReader reader = new PrologReader();
		reader.readFromFile(intro_file_path, theory);

		ConceptViewer.showConcepts(theory);
		
		
		System.out.println("After loading file");
		TheoryViewer.showTheorySummary(theory);
		Instantiation instantiation = new Instantiation();
		Conjunction conjunction = new Conjunction();
		 Negation2 negation2 = new Negation2();
		 Negation1 negation1 = new Negation1();
		Disjunction disjunction = new Disjunction();
		 Count count = new Count();

		theory.setMaxNonExists(0);
		theory.setMaxEquivs(0);

		disjunction.setNumThreads(8);
		conjunction.setNumThreads(8);
		conjunction.setSearchParameter(
				BinaryProductionRule.THEORY_COPY_STRATEGY,
				BinaryProductionRule.HAVE_SINGLE_THEORY);

		String[] instantiations_allowed = { "symbol" };
		for (String ins_allowed : instantiations_allowed) {
			instantiation.addSearchParameter(
					Instantiation.ADD_ALLOWED_INSTANTIATION_TYPE, ins_allowed);
		
			
		}

		long ms2 = System.currentTimeMillis();


				
		instantiation.applyToTheory(theory);
		conjunction.applyToTheory(theory);
		 negation1.applyToTheory(theory);
		 //count.applyToTheory(theory);
		 negation2.applyToTheory(theory);
		  disjunction.applyToTheory(theory);
		 //conjunction.applyToTheory(theory);
		 //conjunction.applyToTheory(theory);


		long ms3 = System.currentTimeMillis();

		// NonExistenceViewer.showNonExistences(theory,
		// theory.getNonExistences(), TheoryViewer.PROLOG_LANGUAGE);

		long ms4 = System.currentTimeMillis();

		long num_steps = theory.getNumberOfSteps();
		long steps_per_second = 1000l * ((long) num_steps / (ms3 - ms2));
		
	
		
		System.out.println(Math.round(ms4 - ms1) + "ms taken overall");
		System.out.println(theory.getMemoryUsage() + " Gb of memory used");
		System.out.println("Running at " + steps_per_second
				+ " steps per second");

		ConceptViewer.showConcepts(theory);
	}
}
