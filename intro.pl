concept(c1,[A,B]) :- goal(name,A,B).
concept(c2,[A]) :- type(A).
concept(c3,[A,B]) :- goal(A), label(A,B).
concept(c4,[A,B]) :- label(A,B), type(B), positive(A,B).
concept(c5,[A,B]) :- label(A,B), type(B), negative(A,B).


label(g1,imp).
label(g2,and).
label(g3,or).
label(g4,imp).
label(g4a,and).
label(g4b,or).
label(g5,or).
label(g5a,or).
label(g6,imp).
label(g6a,imp).
label(g7,and).
label(g8,imp).
label(g9,or).
label(g10,and).

goal(g1,A,A).
goal(g2,B,A).
goal(g3,A,B).
goal(g4,g4a,g4b).
goal(g4a,A,B).
goal(g4b,A,B).
goal(g5,A,g5a).
goal(g5a,B,C).
goal(g6,A,g6a).
goal(g6a,B,A).
goal(g7,A,A).
goal(g8,g8a,A).
goal(g9,A,g9a).
goal(g10,A,B).



positive(g1,t2).
positive(g2,t1).
positive(g3,t3).
positive(g4,t2).
positive(g5,t3).
positive(g6,t2).
positive(g7,t1).
positive(g8,t2).
positive(g9,t3).
positive(g10,t1).



negative(g1,t1).
negative(g1,t3).
negative(g2,t2).
negative(g2,t3).
negative(g3,t1).
negative(g3,t2).
negative(g4,t1).
negative(g4,t3).
negative(g5,t1).
negative(g5,t2).
negative(g6,t1).
negative(g6,t3).
negative(g7,t2).
negative(g7,t3).
negative(g8,t1).
negative(g8,t3).
negative(g9,t1).
negative(g9,t2).
negative(g10,t2).
negative(g10,t3).



concept(c1,[A]) :- term(A).
concept(c2,[A]) :- symbol(A).
concept(c3,[A,B,C,D]) :- symbol(A), term(B), term(C), term(D).
concept(c4,[A]) :- goaltype(A).
concept(c5,[A,B]) :- term(A), goaltype(B), of_gt(A,B).
symbol(and).
symbol(or).
symbol(imp).
term(x).
term(g1).
term(g2).
goaltype(t1).

c3(and,A,A,g1).
c3(or,B,C,x).
c3(and,A,x,g2).

of_gt(g1,t1).
of_gt(g2,t1).