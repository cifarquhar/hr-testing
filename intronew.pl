
concept(c1,[A]) :- term(A).
concept(c2,[A]) :- symbol(A).
concept(c3,[A]) :- term(A), label(A).
concept(c4,[A,B,C,D]) :- symbol(A), term(B), term(C), label(D), goal(A,B,C,D).
concept(c5,[A]) :- term(A), of_gt(A).


term(A).
term(B).
term(w).
term(x).
term(y).
term(z).

label(g1).
label(g2).
label(g3).

symbol(and).
symbol(or).
symbol(imp).


goal(imp,A,A,g1).
goal(imp,x,y,g2).
goal(and,A,B,x).
goal(or,A,B,y).
goal(imp,A,z,g3).
goal(imp,B,A,z).


of_gt(g1).
of_gt(g2).
of_gt(g3).

